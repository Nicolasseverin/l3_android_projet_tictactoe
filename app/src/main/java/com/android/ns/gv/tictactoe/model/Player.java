package com.android.ns.gv.tictactoe.model;

import android.view.View;
import java.util.ArrayList;

/** Model Class: Player
 * Classe abstraite "Player" qui est redéfinie dans les différentes classes "filles" : HumanPlayer et ComputerPlayer
 * Contient le symbole (X ou O)
 * Contient les cellules choisies jusqu'à présent par un joueur.
 * Author: NS & GV
 */

public abstract class Player {
    protected boolean active;
    protected ArrayList<Integer> cellsChosen = new ArrayList<>(); // Liste des cellules contenant les symboles du joueur
    protected Board board;
    protected int playerMark;

    // Constructeur de la classe Player
    public Player(Board board, int playerMark) {
        this.board = board;
        this.playerMark = playerMark;
    }

    // Méthode get qui renvoie la liste des cellules contenant les symboles du joueur
    public ArrayList<Integer> getCellsChosen() {
        return cellsChosen;
    }

    // Cette méthode ajoute la cellule venant d'etre remplie par le joueur a sa liste contenant toutes ses cellules jouées
    public void addChosenCell(Integer cellNumber) {
        cellsChosen.add(cellNumber);
    }

    // Cette méthode renvoie un booleen déterminant si c'est le tour du joueur ou non
    public boolean isActive() {
        return active;
    }

    // Cette méthode modifie l'état de la variable active (Cette variable determine si c'est le tour du joueur ou non)
    public void setActive(boolean active) {
        this.active = active;
    }

    public abstract void playTurn(View view); // Cette méthode est abstraite afin d'etre redéfinie dans les classes "filles" : HumanPlayer et ComputerPlayer
}