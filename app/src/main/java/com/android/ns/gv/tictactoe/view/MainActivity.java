package com.android.ns.gv.tictactoe.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.ns.gv.tictactoe.R;

/**
 * View Class: MainActivity
 * Les differentes view selon le mode de jeu
 * Author: NS & GV
 */
public class MainActivity extends AppCompatActivity {
    Button button1;
    Button button2;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
    }

    public void solo_click(View view) {
        intent = new Intent(MainActivity.this, GameActivity.class);
        intent.putExtra(getString(R.string.intent_extra_key_solo_game_mode), true);
        startActivity(intent);
    }

    public void multiplayer_click(View view) {
        intent = new Intent(MainActivity.this, GameActivity.class);
        intent.putExtra(getString(R.string.intent_extra_key_solo_game_mode), false);
        startActivity(intent);
    }
}