package com.android.ns.gv.tictactoe.controller;

import android.view.View;
import com.android.ns.gv.tictactoe.model.Board;
import com.android.ns.gv.tictactoe.model.Player;
import java.util.ArrayList;

/**
 * Controller Class: GameController
 * Alterne les tours entre les joueurs et leur demande d'effectuer une action.
 * Connaît les combinaisons gagnantes (en utilisant la structure du tableau) interrogée à partir de View Class..
 * Author: NS & GV
 */

public class GameController {
    private Player Player1;
    private Player Player2;
    private Board board;

    // Constructeur de la classe GameController
    public GameController(Board board, Player player1, Player player2) {
        this.board = board;
        this.Player1 = player1;
        this.Player2 = player2;
    }

    // Méthode qui défini le déroulement d'une partie multijoueur
    public void playMultiMode(View view) {
        if (Player1.isActive()) { // C'est au joueur 1 de jouer
            Player1.playTurn(view); // La methode playTurn() permet au joueur de faire son mouvement (poser un symbole)
            Player2.setActive(true); // Le joueur 2 jusqu'a present inactif, passe en actif
            Player1.setActive(false); // On passe le joueur 1 en mode inactif pour passer au tour du joueur 2

        } else if (Player2.isActive()) { // C'est au joueur 2 de jouer, meme principe que pour le joueur 1
            Player2.playTurn(view);
            Player1.setActive(true);
            Player2.setActive(false);
        }
    }

    // Méthode qui défini le déroulement d'une partie solo
    public void playSoloMode(View view) {
        if (Player1.isActive()) { // C'est au joueur 1 de jouer, meme principe que pour le mode multijoueur
            Player1.playTurn(view);
            Player2.setActive(true);
            Player1.setActive(false);
            // On rappel la méthode pour declancher le tour de l'IA
            playSoloMode(view);

            // En plus de vérifier si c'est le tour de l'IA, on vérifie aussi que le joueur 1 n'a pas gagner pour ne pas effectuer un mouvement de trop
        } else if (Player2.isActive() && isPlayerWinner(Player1) == false) {
            Player2.playTurn(view);
            Player2.setActive(false);
            Player1.setActive(true);
        }
    }

    // Cette méthode vérifie si la grille est remplie (ce qui revient a une égalité)
    public boolean isTieGame() {
        return Player1.getCellsChosen().size() + Player2.getCellsChosen().size() == Board.BOARD_CELLS_NUMBER;
    }

    // Cette méthode vérifie si le joueur a gagné en comparant la liste des cellules où il a placé ses symboles et la liste des combinaisons gagnantes de cellules
    public boolean isPlayerWinner(Player player) {
        for (ArrayList<Integer> winningArray : board.getWinningCells())
            if (player.getCellsChosen().containsAll(winningArray))
                return true;
        return false;
    }
}
