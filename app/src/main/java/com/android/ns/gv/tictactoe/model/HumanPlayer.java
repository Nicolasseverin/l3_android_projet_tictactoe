package com.android.ns.gv.tictactoe.model;

import android.view.View;
import android.widget.Button;

/**
 * Model Class: HumanPlayer
 * Défini le tour d'un joueur humain
 * Author: NS & GV
 */

public class HumanPlayer extends Player {

    // Constructeur de la classe HumanPlayer qui hérite de la classe Player
    public HumanPlayer(Board board, int playerMark) {
        super(board, playerMark);
    }

    @Override
    public void playTurn(View view) {
        Button buttonSelected = (Button) view;
        // Désactive le bouton pour éviter la possibilité de re-cliquer sur ce dernier
        buttonSelected.setEnabled(false);
        // Affiche le symbole du joueur sur la cellule
        buttonSelected.setBackgroundResource(playerMark);
        // Ajoute la cellule a la liste des cellules occupé par le joueur et la retire de la liste des cellules vides
        addChosenCell(buttonSelected.getId());
        board.occupyCell(buttonSelected.getId());
    }
}