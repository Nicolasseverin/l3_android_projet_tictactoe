package com.android.ns.gv.tictactoe.model;

import android.view.View;
import android.widget.Button;
import java.util.Random;

/**
 * Controller Class: ComputerPlayer
 * Une IA simple qui choisi aléatoirement une cellules pour y placer son symbole
 * Author: NS & GV
 */

public class ComputerPlayer extends Player {

    // Constructeur de la classe ComputerPlayer qui hérite de la classe Player
    public ComputerPlayer(Board board, int playerMark) {
        super(board, playerMark);
    }
    // création d'un objet Random qui servira plus tard a créer un nombre aléatoire
    Random rand = new Random();

    @Override
    public void playTurn(View view) {
        // S'assure que l'IA n'effectue une action interdite

        // Initialisation d'une variable contenant un nombre aléatoire entre 0 et le nombre de cellules vides
        int int_random = rand.nextInt(board.getEmptyCells().size());

        if (board.getEmptyCells().size() > 1) {
            // Choisi aléatoirement une cellule dans laquelle placer son symbole parmis les cellules vides
            int buttonID = board.getEmptyCells().get(int_random);

            Button buttonSelected = view.getRootView().findViewById(buttonID);
            // Désactive le bouton pour éviter la possibilité de re-cliquer sur ce dernier
            buttonSelected.setEnabled(false);
            // Affiche le symbole du joueur sur la cellule sélectionnée
            buttonSelected.setBackgroundResource(playerMark);
            // Ajoute la cellule a la liste des cellules occupé par le joueur et la retire de la liste des cellules vides
            addChosenCell(buttonID);
            board.occupyCell(buttonID);
        }
    }

}