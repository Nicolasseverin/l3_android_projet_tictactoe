package com.android.ns.gv.tictactoe.model;

import com.android.ns.gv.tictactoe.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Controller Class: Board
 * Contient les informations de la structure de la grille ainsi que les identifiants des cellules
 * Surveille et actualise les changements des cellules vides
 * Author: NS & GV
 */
public class Board {
    private ArrayList<Integer> emptyCells = new ArrayList<>(); // Liste des cellules de la grille qui sont vides
    private ArrayList<ArrayList<Integer>> winningCells = new ArrayList<>(); // Liste des combinaisons gagnantes de cellules
    public static int BOARD_CELLS_NUMBER = 9; // Nombre de cellule total

    // Constructeur de la classe Board
    public Board() {
        // On initialise les listes
        initEmptyCells();
        initWinningCells();
    }

    // Méthode get qui renvoie la liste des cellules vides
    public ArrayList<Integer> getEmptyCells() {
        return emptyCells;
    }

    // Méthode get qui renvoie la liste des combinaisons gagnantes de cellules
    public ArrayList<ArrayList<Integer>> getWinningCells() {
        return winningCells;
    }

    // Cette méthode retire de la liste des cellules vides, une cellule qui vient d'etre remplie
    public void occupyCell(Integer buttonID) {
        if (emptyCells.size() >= 1)
            emptyCells.remove(buttonID);
    }
    // Cette méthode initialise la liste cellules vides en ajoutant chacunes des 9 cellules de la grille a chaque début de partie
    private void initEmptyCells() {
        emptyCells.add(R.id.button1);
        emptyCells.add(R.id.button2);
        emptyCells.add(R.id.button3);
        emptyCells.add(R.id.button4);
        emptyCells.add(R.id.button5);
        emptyCells.add(R.id.button6);
        emptyCells.add(R.id.button7);
        emptyCells.add(R.id.button8);
        emptyCells.add(R.id.button9);
    }

    // Cette méthode initialise la liste des combinaisons gagnantes de cellules
    private void initWinningCells() {
        // Combinaisons de lignes gagnantes
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button1, R.id.button2, R.id.button3)));
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button4, R.id.button5, R.id.button6)));
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button7, R.id.button8, R.id.button9)));
        // Combinaisons de colonnes gagnantes
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button1, R.id.button4, R.id.button7)));
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button2, R.id.button5, R.id.button8)));
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button3, R.id.button6, R.id.button9)));
        // Combinaisons de diagonales gagnantes
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button1, R.id.button5, R.id.button9)));
        winningCells.add(new ArrayList<>(Arrays.asList(R.id.button3, R.id.button5, R.id.button7)));
    }
}